#include <iostream>
using namespace std;
//progam menghitung luas permukaan limas persegi panjang dengan tinggi pasangan sisi segitiga yang berhadapan sama panjangnya

int main(){
	int tinggi1;
	int panjang;
	int lebar;
	int tinggi2;
	int luasSegitiga1;
	int luasSegitiga2;
	int luasPersegipanjang;
	int luas;
	
	cout<<"Masukkan tinggi segitiga dari sisi panjang alas: ";cin>>tinggi1;
	cout<<"Masukkan tinggi segitiga dari sisi lebar alas: ";cin>>tinggi2;
	cout<<"Masukkan panjang alas : ";cin>>panjang;
	cout<<"Masukkan lebar alas : ";cin>>lebar;
	
	luasSegitiga1 = tinggi1*panjang/2;
	luasSegitiga2 = tinggi2*lebar/2;
	
	luasPersegipanjang = panjang*lebar;
	
	luas = luasPersegipanjang+2*luasSegitiga1+2*luasSegitiga2;
	
	cout<<"Luas permukaan limas persegi adalah :  "<<luas<<"cm2"<<endl;
	
	
}
