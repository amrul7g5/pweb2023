#include <iostream>
using namespace std;
//Progam input data pada kartu tanda penduduk (KTP)

int main(){
	string provinsi,kabupaten,NIK,nama,tempat,tanggal,kelamin,goldar,alamat,RT,RW,desa,kecamatan,agama,kawin,pekerjaan,kewarganegaraan;
	
	cout<<"------------Progam pengisian data kartu tanda penduduk (KTP)--------------"<<endl;
	cout<<"Masukkan Provinsi 		: ";getline(cin,provinsi);
	cout<<"Masukkan Kabupaten 		: ";cin>>kabupaten;
	cout<<"Masukkan NIK			: ";cin>>NIK;
	cout<<"Masukkan nama			: ";cin.ignore();getline(cin,nama);
	cout<<"Masukkan tempat lahir		: ";cin>>tempat;
	cout<<"Masukkan tanggal lahir		: ";cin>>tanggal;
	cout<<"Masukkkan jenis kelamin		: ";cin>>kelamin;
	cout<<"Masukkan golongan darah		: ";cin>>goldar;
	cout<<"Masukkan alamat			: ";cin.ignore();getline(cin,alamat);
	cout<<"Masukkan RT			: ";cin>>RT;
	cout<<"Masukkan RW			: ";cin>>RW;
	cout<<"Masukkan desa 			: ";cin>>desa;
	cout<<"Masukkan Kecamatan		: ";cin>>kecamatan;
	cout<<"Masukkan agama			: ";cin>>agama;
	cout<<"Masukkan status kawin		: ";cin>>kawin;
	cout<<"Masukkan pekerjaan		: ";cin>>pekerjaan;
	cout<<"Masukkan kewarganegaraan	: ";cin>>kewarganegaraan;
	cout<<endl;
	cout<<"Apakah data yang dimasukkan sudah benar?"<<endl;
	system("pause");
	system("cls");
	
	cout<<" _______________________________________________________________________________________"<<endl;
	cout<<"			     	Provinsi "<<provinsi<<endl;
	cout<<"				 Kabupaten "<<kabupaten<<endl;
	cout<<"											"<<endl;
	cout<<"NIK					: "<<NIK<<endl;
	cout<<"Nama					: "<<nama<<endl;
	cout<<"Tempat/tanggal lahir			: "<<tempat<<","<<tanggal<<endl;
	cout<<"Jenis Kelamin				: "<<kelamin<<"			"<<"Gol. Darah : "<<goldar<<endl;
	cout<<"Alamat					: "<<alamat<<endl;
	cout<<"\tRT/RW				: "<<RT<<"/"<<RW<<endl;
	cout<<"\tKelurahan/Desa			: "<<desa<<endl;
	cout<<"\tKecamatan			: "<<kecamatan<<endl;
	cout<<"Agama					: "<<agama<<endl;
	cout<<"Status Perkawinan			: "<<kawin<<endl;
	cout<<"Pekerjaan				: "<<pekerjaan<<endl;
	cout<<"Kewarganegaraan				: "<<kewarganegaraan<<endl;
	cout<<"Berlaku Hingga				: SEUMUR HIDUP			"<<endl;
	cout<<" _______________________________________________________________________________________"<<endl;

}
