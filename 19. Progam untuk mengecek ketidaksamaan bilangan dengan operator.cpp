#include <iostream>
using namespace std;
//progam untuk mengecek ketidaksamaan bilangan dengan operator

int main(){
	int bil1,bil2,bil3;
	
	cout<<"Masukkan bilangan pertama 	: ";cin>>bil1;
	cout<<"Masukkan bilangan kedua 	: ";cin>>bil2;
	cout<<"Masukkan bilangan ketiga 	: ";cin>>bil3;
	cout<<endl;
	cout<<"Jika bernilai 1 maka true,jika bernilai 0 maka false"<<endl;
	
	cout<<"Bilangan pertama ("<<bil1<<") tidak sama dengan bilangan kedua ("<<bil2<<") 	: "<<(bil1!=bil2)<<endl;
	cout<<"Bilangan pertama ("<<bil1<<") tidak sama dengan bilangan ketiga ("<<bil3<<") 	: "<<(bil1!=bil3)<<endl;
	cout<<"Bilangan kedua ("<<bil2<<") tidak sama dengan bilangan ketiga ("<<bil3<<") 	: "<<(bil2!=bil3)<<endl;
}
